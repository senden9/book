# Download

Read this page carefully for a detailed description on how to run Veloren and to avoid common pitfalls.

- If you're only interested in playing Veloren, please read the pages [Download Releases](./releases.md), [Dependencies](./dependencies.md) and [Run](./run.md), you can skip the Toolchain and Compiling from Source pages.
- If you want to contribute to Veloren you need to compile it yourself, follow the pages [Toolchain](download/toolchain.md), [Compiling from Source](./compiling.md), [Dependencies](./dependencies.md) and [Run](./run.md).
